### The updating of ecs clusters container image immediatelly (1 sec)

PS: On MacOS this script is not working correctly. You can use for example Ubuntu for launching it.

```
$ pip install awscli
$ cp config.inc.example config.inc
```

Connect to your AWS account by some access_key and secret_key by launch the next command:
```
$ aws configure
AWS Access Key ID [****************KAEA]: 
AWS Secret Access Key [****************NYgs]: 
Default region name [eu-west-1]: 
Default output format [json]: json
```

Login to your private docker registry on AWS
```
$ aws ecr get-login --region eu-west-1
$ < put here the output of above command >
```

Edit the config.inc file

Launch the script
```
$ ./update_ecs_task.sh
```