#!/bin/bash

set -e 

if [ ! -z "${CONFIG_FILE}" ] && [ -e "${CONFIG_FILE}" ]; then
    echo "Setting a new config file: ${CONFIG_FILE}"
else
    CONFIG_FILE="config.inc"
fi

echo ${CONFIG_FILE}

which aws &> /dev/null

if [ $? != 0 ]; then
    echo -e "\nThe script can not find the AWS CLI.\nPlease install it by launching the command: [ pip install awscli ]\n"
    exit 1
fi 

if [ ! -e "${CONFIG_FILE}" ]; then
    echo -e "\nThe script can not read the [ config.inc ] file which shoud be located by path [ $(dirname $0)/${CONFIG_FILE} ]\n"
    exit 1
fi

source ${CONFIG_FILE}

# Checking the input VARIABLES
if [ -z "${CLUSTER}" ] || [ -z "${SERVICES}" ]; then
    echo -e "\nNot all necessary variables were declared!\n"
    exit 1
fi


for SERVICE in ${SERVICES}; do
    echo "Current service: ${SERVICE}"
    
    # Find the backup service and launch it
    BACKUP_SERVICE="${SERVICE}${SUFFIX}"
    if [ "$(aws ecs list-services --cluster ${CLUSTER} | grep "${BACKUP_SERVICE}")" != "" ]; then
        printf "Launching the backup-service"
        aws ecs update-service --cluster ${CLUSTER} --service ${BACKUP_SERVICE} --desired-count 1 > /dev/null
        while  [ "$(aws ecs describe-services --cluster ${CLUSTER} --service ${BACKUP_SERVICE} | grep runningCount | head -1 | sed -r "s/ //g" | awk -F ":" '{ print $2 }')" == "0" ]; do
            printf "."
            sleep 1
        done
    else 
        echo "Can not find the backup service!..."
    fi 
    # 
    echo 
    INSTANCES=""
    for TASK_ARN in $(aws ecs list-tasks --cluster ${CLUSTER} --service ${BACKUP_SERVICE} --desired-status RUNNING | grep "arn:"); do
        TASK_ID=$(echo ${TASK_ARN} | sed -r "s/ //g")
	TASK_ID=$(echo ${TASK_ARN} | cut -d / -f 2 | cut -d \" -f 1)
	TASK_INSTANCE=$(aws ecs describe-tasks --cluster ${CLUSTER} --tasks ${TASK_ID} | grep "containerInstanceArn" | awk -F ": " '{ print $2 }' )
        # Del the last char [,] in ARN        
        TASK_INSTANCE=$(echo ${TASK_INSTANCE} | sed -r 's/.$//' | cut -d / -f 2 | cut -d \" -f 1)
        TASK_INSTANCE=$(aws ecs describe-container-instances --cluster ${CLUSTER} --container-instances ${TASK_INSTANCE} | grep ec2InstanceId | awk -F ": " '{ print $2 }' | awk -F "," '{ print $1 }' | awk -F "\"" '{ print $2 }')
        if [ "${TASK_INSTANCE}" != "" ]; then
            INSTANCES="${TASK_INSTANCE} ${INSTANCES}"
        fi
    done    
    
#exit
####
####
    # Force to stop services tasks
    printf "\nStopping the service."
    DESIRED_COUNT=$(aws ecs describe-services --cluster ${CLUSTER} --service ${SERVICE} | grep runningCount | head -1 | sed -r "s/ //g" | awk -F ":" '{ print $2 }')
    aws ecs update-service --cluster ${CLUSTER} --service ${SERVICE} --desired-count 0 > /dev/null
    for TASK_ARN in $(aws ecs list-tasks --cluster ${CLUSTER} --service ${SERVICE} --desired-status RUNNING | grep "arn:"); do
	TASK_ID=$(echo ${TASK_ARN} | sed -r "s/ //g")
	TASK_ID=$(echo ${TASK_ARN} | cut -d / -f 2 | cut -d \" -f 1)
	aws ecs stop-task --cluster ${CLUSTER} --task ${TASK_ID} > /dev/null
    done    
    while [ "$(aws ecs describe-services --cluster ${CLUSTER} --service ${SERVICE} | grep runningCount | head -1 | sed -r "s/ //g" | awk -F ":" '{ print $2 }')" != "0" ]; do
	printf "."
	sleep 1
    done

    printf "\nRunning the service."
    aws ecs update-service --cluster ${CLUSTER} --service ${SERVICE} --desired-count ${DESIRED_COUNT} > /dev/null
    while [ "$(aws ecs describe-services --cluster ${CLUSTER} --service ${SERVICE} | grep runningCount | head -1 | sed -r "s/ //g" | awk -F ":" '{ print $2 }')" == "0" ]; do
	printf "."
	sleep 1
    done
    if [ "$(aws ecs list-services --cluster ${CLUSTER} | grep "${BACKUP_SERVICE}")" != "" ]; then
        printf "\nStopping the backup-service"
        aws ecs update-service --cluster ${CLUSTER} --service ${BACKUP_SERVICE} --desired-count 0 > /dev/null
        echo 
    fi 
done
